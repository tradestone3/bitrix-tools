<?php

namespace Devaccess\Tools\Iblock;

/**
 * Класс "Пример класса инфоблока"
 */
class Objects extends Prototype
{
    /**
     * Возвращает "Пример класса инфоблока"
     *
     * @return Example
     */
    public static function getInstance()
    {
        return parent::getInstance();
    }
}