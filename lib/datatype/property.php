<?php
namespace Devaccess\Tools\DataType;

/**
 * Кастомный тип данных "привязка к свойству инфоблока"
 */
class Property {
	/**
	 * Регистрируем обработчик события главного модуля OnUserTypeBuildList
	 * Событие создается при построении списка типов пользовательских свойств
	 *
	 * @return mixed
	 */
	public static function GetUserTypeDescription() {
		return [
			'USER_TYPE_ID' => 'affiliat',
			'CLASS_NAME' => '\Devaccess\Tools\DataType\Property',
			'DESCRIPTION' => 'Привязка к свойству инфоблока',
			'BASE_TYPE' => 'int',
		];
	}

	/**
	 * Получает список свойств инфоблока
	 *
	 * @param $iblockId $number Число
	 * @return mixed
	 */
	private static function getProps($iblockId) {
		$properties = \CIBlockProperty::GetList(["sort" => "asc", "name" => "asc"], [
				"ACTIVE" => "Y",
				"IBLOCK_ID" => $iblockId
		]);

		return $properties;
	}

	/**
	 * Функция вызывается при добавлении нового свойства
	 * для конструирования SQL запроса создания столбца значений свойства
	 *
	 * @param integer $arUserField Число
	 * @param boolean $incNum Добавить само число в результат
	 * @return string
	 */
	public static function GetDBColumnType($arUserField) {
		switch(strtolower($GLOBALS['DB']->type)) {
			case 'mysql':
				return 'int(18)';
			case 'oracle':
				return 'number(18)';
			case 'mssql':
				return 'int';
		}
	}

	/**
	 * Функция вызывается перед сохранением метаданных (настроек) свойства в БД
	 *
	 * @param integer $arUserField Число
	 * @return array массив уникальных метаданных для свойства, будет сериализован и сохранен в БД
	 */
	public static function PrepareSettings($arUserField) {
		// инфоблок, с элементами которого будет выполняться связь
		$iIBlockId = intval($arUserField['SETTINGS']['IBLOCK_ID']);
		return [
			'IBLOCK_ID' => $iIBlockId > 0 ? $iIBlockId : 0
		];
	}

	/**
	 * Функция вызывается при выводе формы метаданных (настроек) свойства
	 *
	 * @param bool $arUserField
	 * @param $arHtmlControl
	 * @param $bVarsFromForm
	 * @return string
	 */
	public static function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm) {
		$result = '';

		if(!\CModule::IncludeModule('iblock')) {
			return $result;
		}

		// текущие значения настроек
		if($bVarsFromForm) {
			$value = $GLOBALS[$arHtmlControl['NAME']]['IBLOCK_ID'];
		}
		elseif(is_array($arUserField)) {
			$value = $arUserField['SETTINGS']['IBLOCK_ID'];
		}
		else {
			$value = '';
		}

		// выведем выпадающий список выбора связываемого инфоблока
		$result .= '
		<tr style="vertical-align: top;">
			<td>Информационный блок по умолчанию:</td>
			<td>
			'.GetIBlockDropDownList($value, $arHtmlControl['NAME'].'[IBLOCK_TYPE_ID]', $arHtmlControl['NAME'].'[IBLOCK_ID]').'
			</td>
		</tr>
		';

		return $result;
	}

	/**
	 * Функция вызывается при выводе формы метаданных (настроек) свойства
	 *
	 * @param integer $arUserField Число
	 * @param boolean $arHtmlControl - флаг отправки формы
	 * @return string HTML для вывода
	 */
	public static function GetEditFormHTML($arUserField, $arHtmlControl) {
		ob_start();
		?>
		<select name="<?=$arHtmlControl['NAME']?>">
			<option value="">
				Выберите свойство
			</option>
			<?

			if($res = self::getProps($arUserField["SETTINGS"]["IBLOCK_ID"])) {
				while($row = $res->GetNext()) {
					?>
					<option value="<?=$row['ID']?>"<? if($arHtmlControl['VALUE'] == $row['ID']) { ?> selected="selected"<?
					} ?>>
						[<?=$row['NAME']?>]
					</option>
					<?
				}
			}
			?>
		</select>
		<?
		$sField = ob_get_contents();
		ob_end_clean();
		return $sField;
	}

	/**
	 * Функция вызывается при выводе фильтра на странице списка
	 *
	 * @param integer $arUserField Число
	 * @param boolean $arHtmlControl - флаг отправки формы
	 * @return string HTML для вывода
	 */
	public static function GetFilterHTML($arUserField, $arHtmlControl) {
		return '';
	}

	/**
	 * Функция вызывается при выводе значения множественного свойства в списке элементов
	 *
	 * @param integer $arUserField Число
	 * @param boolean $arHtmlControl - флаг отправки формы
	 * @return string HTML для вывода
	 */
	public static function GetAdminListViewHTML($arUserField, $arHtmlControl) {
		$sField = '';
		if($arHtmlControl['VALUE'] > 0) {
			$sField .= '<a href="sale_affiliate_edit.php?ID='.$arHtmlControl['VALUE'].'&amp;lang='.LANGUAGE_ID.'">'.$arHtmlControl['VALUE'].'</a>';
		}
		return $sField;
	}

	/**
	 * Функция валидатор значений свойства
	 * вызвается в $GLOBALS['USER_FIELD_MANAGER']->CheckField() при добавлении/изменении
	 *
	 * @param array $value значение для проверки на валидность
	 * @return array массив массивов ("id","text") ошибок
	 */
	public static function CheckFields($arUserField, $value) {
		$aMsg = [];
		return $aMsg;
	}

	/**
	 * Функция должна вернуть представление значения поля для поиска
	 *
	 * @param array
	 * @return string - посковое содержимое
	 */
	public static function OnSearchIndex($arUserField) {
		return '';
	}
}