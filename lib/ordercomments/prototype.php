<?php

namespace Devaccess\Tools\Ordercomments;

use Devaccess\Tools\Exception;

class Prototype
{
    /**
     * ID заказа
     *
     * @var integer
     */
    protected $orderId = 0;

    /**
     * Экземпляр класса CIBlockElement
     *
     * @var \Bitrix\Sale\Order
     */
    public $order = null;

    /**
     * Конструктор
     *
     * @param integer $id ID инфоблока
     * @throws Exception
     */
    protected function __construct($orderId = 0)
    {
        if ($orderId) {
            $this->orderId = $orderId;
        }

        $this->order = \Bitrix\Sale\Order::load($orderId);

        if (!$this->orderId) {
            throw new Exception('Order ID is undefined.');
        }
    }

    /**
     * Запись комментария в заказ
     *
     * @param string $comment Комментарий
     * @throws Exception
     */
    public function setComment($comment)
    {
        if (!empty($comment)) {
            $this->order->setField('USER_DESCRIPTION', $comment);
        } else {
            throw new Exception('Comment is empty.');
        }
    }
}