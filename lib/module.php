<?php
/**
 * Devaccess.tools module
 *
 * @category    devaccess
 * @link        https://devaccess.ru
 */
namespace Devaccess\Tools;

use Bitrix\Main\EventManager;
use Devaccess\Tools\DataType\Property;

/**
 * Основной класс модуля
 */
class Module
{
    /**
     * Обработчик начала отображения страницы
     *
     * @return void
     */
    public static function onPageStart()
    {
        self::checkTwoLevelsArchitecture();
        self::defineConstants();
        self::includeExtLibs();
    }


    /*
     * Переключение режима (Частным/Юрлицам)
     */
    protected static function changeUserType()
    {

    }

    /**
     * Проверяет конфигурацию в случае 2-уровневой архитектуры (nginx -> apache)
     *
     * @return void
     */
    protected static function checkTwoLevelsArchitecture()
    {
        if ($_SERVER['HTTP_X_FORWARDED_FOR'] && $_SERVER['REMOTE_ADDR'] == '172.16.32.1') {
            if ($p = strrpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')) {
                $_SERVER['REMOTE_ADDR'] = $REMOTE_ADDR = trim(substr($_SERVER['HTTP_X_FORWARDED_FOR'], $p + 1));
                $_SERVER['HTTP_X_FORWARDED_FOR'] = substr($_SERVER['HTTP_X_FORWARDED_FOR'], 0, $p);
            } else {
                $_SERVER['REMOTE_ADDR'] = $REMOTE_ADDR = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
    }

    /**
     * Задает константы сущностей
     */
    protected static function defineConstants()
    {
        define(__NAMESPACE__ . '\IS_INDEX', \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestedPage() == '/index.php');
        define(__NAMESPACE__ . '\IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');

        Hlblock\Prototype::defineConstants();
        Iblock\Prototype::defineConstants();
    }

    /**
     * Подключает внешние библиотеки из composer
     */
    protected static function includeExtLibs()
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php')) {
            require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
        }
    }

}