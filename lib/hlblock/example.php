<?php

namespace Devaccess\Tools\Hlblock;

/**
 * Класс "Пример класса highload-блока"
 */
class Example extends Prototype
{

	/**
	 * Возвращает hlblock "Пример класса справочник"
	 *
	 * @return Example
	 */
	public static function getInstance()
	{
		return parent::getInstance();
	}
}