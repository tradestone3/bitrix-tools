<?php

namespace Devaccess\Tools;

/**
 * Утилиты
 */
class Util
{
    /**
     * Склоняет существительное с числительным
     *
     * @param integer $number Число
     * @param array $cases Варианты существительного в разных падежах и числах (nominativ, genetiv, plural). Пример: array('комментарий', 'комментария', 'комментариев')
     * @param boolean $incNum Добавить само число в результат
     * @return string
     */
    public static function getNumEnding($number, $cases, $incNum = true)
    {
        $numberMod = intval(preg_replace('/[^0-9.,]/', '', $number)) % 100;
        if ($numberMod >= 11 && $numberMod <= 19) {
            $result = $cases[2];
        } else {
            $numberMod = $numberMod % 10;
            switch ($numberMod) {
                case 1:
                    $result = $cases[0];
                    break;
                case 2:
                case 3:
                case 4:
                    $result = $cases[1];
                    break;
                default:
                    $result = $cases[2];
            }
        }

        return $incNum ? $number . ' ' . $result : $result;
    }

    /**
     * Обрезает текст, превыщающий заданную длину
     *
     * @param string $text Текст
     * @param array $config Конфигурация
     * @return string
     */
    public static function getEllipsis($text, $config = [])
    {
        $config = array_merge([
            'mode' => 'word',
            'count' => 255,
            'suffix' => '&hellip;',
            'stripTags' => true,
        ], $config);

        if ($config['stripTags']) {
            $text = preg_replace([
                '/(\r?\n)+/',
                '/^(\r?\n)+/',
            ], [
                "\n",
                '',
            ], strip_tags($text));
        }

        if (strlen($text) > $config['count']) {
            $text = substr($text, 0, $config['count']);
            switch ($config['mode']) {
                case 'direct':
                    break;
                case 'word':
                    $word = '[^ \t\n\.,:]+';
                    $text = preg_replace('/(' . $word . ')$/D', '', $text);
                    break;
                case 'sentence':
                    $sentence = '[\.\!\?]+[^\.\!\?]+';
                    $text = preg_replace('/(' . $sentence . ')$/D', '', $text);
                    break;
            }

            $text = preg_replace('/[ \.,;]+$/D', '', $text) . $config['suffix'];
        }

        if ($config['stripTags']) {
            $text = nl2br($text);
        }
        return $text;
    }

    /**
     * Получает id видео youtube из строки со ссылкой
     *
     * @param $string
     * @return bool
     */

    public static function getYoutubeIDFromString($string)
    {
        if (!strlen($string) > 0) {
            return false;
        }
        $re = "/(?:youtube\\.com\\/(?:[^\\/]+\\/.+\\/|(?:v|e(?:mbed)?)\\/|.*[?&]v=)|youtu\\.be\\/)([^\"&?\\/ ]{11})/i";
        preg_match($re, $string, $matches);
        return $matches[1];
    }

    /**
     * Выводит дамп данных через print_r()
     *
     * @param mixed $data Данные для вывода
     * @return void
     */

    public static function debug($data)
    {
        ?>
        <pre><?= htmlspecialchars(print_r($data, true)); ?></pre><?
    }

    /**
     * Выводит дамп данных через var_dump()
     *
     * @param mixed $data Данные для вывода
     * @return void
     */

    public static function dump($data)
    {
        ?>
        <pre><? var_dump($data); ?></pre><?
    }

    /**
     * Пишет данные в лог
     *
     * @param string $message Данные для вывода
     * @param string $file Имя файла относительно DOCUMENT_ROOT (по-умолчанию log.txt)
     * @param boolean $backtrace Выводить ли информацию о том, откуда был вызван лог
     * @return void
     */
    public static function log($message, $file = '', $backtrace = false) {
        if(!$file) {
            $file = 'log.txt';
        }
        $file = $_SERVER['DOCUMENT_ROOT']."/".$file;
        $text = date('Y-m-d H:i:s').' ';

        if(is_array($message)) {
            $text .= print_r($message, true);
        } else {
            $text .= $message;
        }

        $text .= "\n";
        if($backtrace) {
            $backtrace = reset(debug_backtrace());
            $text = "Called in file: ".$backtrace["file"]." in line: ".$backtrace["line"]." \n".$text;
        }
        if($fh = fopen($file, 'a')) {
            fwrite($fh, $text);
            fclose($fh);
        }
    }

    /**
     * Формирует строку для вывода размера файла
     *
     * @param integer $bytes Размер в байтах
     * @param integer $precision Кол-во знаков после запятой
     * @param array $types Приставки СИ
     * @return string
     */
    public static function getFileSize($bytes, $precision = 0, array $types = array('B', 'kB', 'MB', 'GB', 'TB'))
    {
        for ($i = 0; $bytes >= 1024 && $i < (count($types) - 1); $bytes /= 1024, $i++) ;

        return round($bytes, $precision) . ' ' . $types[$i];
    }

    /**
     * Заменяет конструкцию #VAR# на значение из массива.
     * Значения "#SITE_DIR#", "#SITE#", "#SERVER_NAME#" заменяются автоматически из текущих значений.
     *
     * @param string $template Шаблон
     * @param array $data Значения для подстановки
     * @param boolean $fixRepeatableSlashes Убирать продублированные слеши
     * @return string
     */
    public static function parseTemplate($template, $data = array(), $fixRepeatableSlashes = true)
    {
        if ($fixRepeatableSlashes) {
            $template = str_replace('//', '#DOUBLE_SLASH#', $template);
        }

        $string = \CComponentEngine::MakePathFromTemplate($template, $data);

        if ($fixRepeatableSlashes) {
            $string = preg_replace('~[/]{2,}~', '/', $string);
            $string = str_replace('#DOUBLE_SLASH#', '//', $string);
        }

        return $string;
    }

    /**
     * Конвертирует кодировку
     *
     * @param mixed $data Данные для кодирования
     * @param string $from Исходная кодировка
     * @param string $to Требуемая кодировка
     * @return mixed
     */
    public static function convertCharset($data, $from, $to)
    {
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data[$key] = self::convertCharset($val, $from, $to);
            }
        } elseif (is_object($data)) {
            foreach ($data as $key => $val) {
                $data->$key = self::convertCharset($val, $from, $to);
            }
        } elseif (is_bool($data) || is_numeric($data)) {
            //do nothing
        } else {
            $data = \CharsetConverter::ConvertCharset($data, $from, $to, $error = '');
        }

        return $data;
    }

    /**
     * Возвращает массив значений указанного ключа исходного массива
     * Например, нужно, чтобы получать из мссива array(array("ID" => 1), array("ID" => 2), array("ID" => 3))
     * массив array(1, 2, 3)
     *
     *
     * @param array $arr
     * @param string $key
     * @param bool $notNull
     * @return array
     */

    public static function getAssocArrItemsKey($arr, $key = "ID", $notNull = false)
    {
        $resArr = array();
        foreach ($arr as $item) {
            if ($notNull && !$item[$key]) {
                continue;
            }
            $resArr[] = $item[$key];
        }
        return $resArr;
    }

    /**
     * Индексирует массив по заданному ключу
     * @param $arr
     * @param string $key
     *
     * @return array
     */
    public static function getIndexedArray($arr, $key = "ID")
    {

        $arRes = array();
        foreach ($arr as $index => $arrItem) {
            $arrItem['INDEX'] = $index;
            $arRes[$arrItem[$key]] = $arrItem;
        }

        return $arRes;
    }

    /**
     * Возвращает элементы многомерного массива, у которых в значения кллюча $key равно $value
     * Работает с массивами с 2-мя уровнями вложенности
     *
     * @param $array
     * @param $key
     * @param $value
     *
     * @return array
     */
    public static function searchInAssocArrayByValue($array, $key, $value)
    {
        $result = [];
        foreach ($array as $item) {
            if($item[$key] == $value) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * Получение пути от $_SERVER['DOCUMENT_ROOT']
     *
     * @param $link
     * @return string
     */

    public static function getRootPath($link)
    {
        return $_SERVER['DOCUMENT_ROOT'] . $link;
    }

    /**
     * Устанавливает значение в Session
     *
     * @param string $code код для ключа сессии
     * @param mixed $value значение переменной сессии
     */

    public static function setSession($code, $value)
    {
        $_SESSION[$code] = $value;
    }

    /**
     * Очищает значение в  Session
     *
     * @param string $code код для ключа сессии
     */

    public static function unsetSession($code)
    {
        unset($_SESSION[$code]);
    }

    /**
     * Получает значение переменной сессии
     *
     * @param string $code код cookie
     * @return mixed
     */

    public static function getSession($code)
    {
        $value = $_SESSION[$code];
        return $value;
    }

    /**
     * Возвращает SECTION_CODE из URL
     */
    public static function getUrlSectionCode()
    {
        $sect_url=array_shift(explode('?', trim($_SERVER['REQUEST_URI'])));
        $sect_code = end(explode('/', trim($sect_url, '/')));
        return $sect_code;
    }
}