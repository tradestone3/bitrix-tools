<?
use Bitrix\Main\Loader;
use Devaccess\Tools\Exception;
use Devaccess\Tools\Mvc\Controller\Prototype as Mvc;
use Bitrix\Main\Application;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$request = Application::getInstance()->getContext()->getRequest();

try {
	if (!Loader::includeModule('devaccess.tools')) {
		throw new Exception('Can\'t include module "devaccess".');
	}
    $name = htmlspecialchars($request->getQuery("controller"));
    $action = htmlspecialchars($request->getQuery("action"));
    $controller = Mvc::factory($name);
	$controller->doAction($action);
} catch(Exception $e) {
	print $e->GetMessage();
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php';