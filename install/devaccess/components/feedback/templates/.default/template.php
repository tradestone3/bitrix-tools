<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
global $APPLICATION;
?>
<? if ($arResult["send"]): ?>
    <? if (!empty($arResult["errors"])): ?>
        <div class="error">
            <? foreach ($arResult["errors"] as $err): ?>
                <?= $err ?><br/>
            <? endforeach ?>
        </div>
    <? else: ?>
        OK
    <? endif ?>
<? else: ?>
    <form id="contact-form" action="<?=$APPLICATION->GetCurPage()?>">
        <div class="input-line">
            <input type="text" name="name">
            <span><?=GetMessage('NAME');?></span>
        </div>
        <div class="input-line">
            <input type="text" name="email">
            <span><?=GetMessage('EMAIL');?></span>
        </div>
        <div class="input-line textarea">
            <textarea name="message" placeholder=""></textarea>
            <span><?=GetMessage('MESSAGE_TEXT');?></span>
        </div>
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="send" value="feedback">
        <input type="submit" value="<?=GetMessage('SEND');?>">
    </form>
<? endif ?>