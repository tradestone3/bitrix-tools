<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\IO\FileDeleteException;
use Bitrix\Main\IO\FileOpenException;
use Bitrix\Main\ModuleManager;

Class devaccess_tools extends CModule
{
    var $MODULE_ID = "devaccess.tools";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    protected $INSTALL_PATHS = [];
    protected $IGNORE_PATTERNS = [];
    protected $INSTALLER_DIR = __DIR__;

    public function __construct()
    {
        $arModuleVersion = [];

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = 'devaccess.tools';
        $this->MODULE_NAME = Loc::getMessage('DEVACCESS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DEVACCESS_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('DEVACCESS_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DEVACCESS_MODULE_PARTNER_URI');

        $this->INSTALLER_DIR = __DIR__;
        $this->INSTALL_PATHS = [
            'bitrix',
            'devaccess',
        ];

        $this->eventHandlers = [
            [
                'main',
                'OnPageStart',
                '\Devaccess\Tools\Module',
                'onPageStart',
            ]
        ];

    }

    private function getRecursiveFiles($path)
    {
        $dirFromDocRoot = substr($this->INSTALLER_DIR, strlen($_SERVER['DOCUMENT_ROOT']));

        $iter = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->INSTALLER_DIR . '/' . $path, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST,
            RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        );

        $result = [];
        /**
         * @var string $filePath
         * @var \SplFileInfo $item
         */
        foreach ($iter as $filePath => $item) {
            if (!$item->isFile() && !$item->isLink()) {
                continue;
            }

            // Skip ignored files
            foreach ($this->IGNORE_PATTERNS as $pattern) {
                if (preg_match($pattern, $filePath) > 0) {
                    continue 2;
                }
            }

            $result[$filePath] = str_replace($dirFromDocRoot, '', $filePath);
        }

        uasort($result, function ($a, $b) {
            return (strlen($a) > strlen($b)) ? -1 : 1;
        });

        return $result;
    }

    private function getRecursiveDirs($path)
    {
        $dirFromDocRoot = substr($this->INSTALLER_DIR, strlen($_SERVER['DOCUMENT_ROOT']));

        $iter = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->INSTALLER_DIR . '/' . $path, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST,
            RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        );

        $result = [];

        if (!in_array($path, ['bitrix', 'local', 'upload'])) {
            $result[] = str_replace($dirFromDocRoot, '', $this->INSTALLER_DIR . '/' . $path);
        }

        foreach ($iter as $filePath => $item) {
            if ($item->isDir()) {
                $result[$filePath] = str_replace($dirFromDocRoot, '', $filePath);
            }
        }

        uasort($result, function ($a, $b) {
            return (strlen($a) > strlen($b)) ? -1 : 1;
        });

        return $result;
    }

    /**
     * @return array
     */
    public function getInstallPaths()
    {
        return $this->INSTALL_PATHS;
    }

    /**
     * @param array $INSTALL_PATHS
     */
    public function setInstallPaths(array $INSTALL_PATHS)
    {
        $this->INSTALL_PATHS = $INSTALL_PATHS;
    }

    /**
     * Устанавливает события модуля
     *
     * @return boolean
     */
    public function installEvents()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();

        foreach ($this->eventHandlers as $handler) {
            $eventManager->registerEventHandler($handler[0], $handler[1], $this->MODULE_ID, $handler[2], $handler[3]);
        }

        return true;
    }

    /**
     * Удаляет события модуля
     *
     * @return boolean
     */
    public function unInstallEvents()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();

        foreach ($this->eventHandlers as $handler) {
            $eventManager->unRegisterEventHandler($handler[0], $handler[1], $this->MODULE_ID, $handler[2], $handler[3]);
        }

        return true;
    }

    public function installDB()
    {
    }

    public function uninstallDB()
    {
    }

    function InstallFiles()
    {
        foreach ($this->INSTALL_PATHS as $path) {
            $files = $this->getRecursiveFiles($path);
            foreach ($files as $from => $to) {
                $dir = dirname($to);
                if (!file_exists($dir)) {
                    if (!mkdir($dir, BX_DIR_PERMISSIONS, true) && !is_dir($dir)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
                    }
                }

                if (is_link($from)) {
                    symlink(readlink($from), $to);
                } else {
                    if (!copy($from, $to)) {
                        throw new FileOpenException($to);
                    }
                }
            }
        }
        return true;
    }

    function UnInstallFiles()
    {
        $isDirEmpty = function ($dir) {
            if (!file_exists($dir) || !is_readable($dir)) {
                return false;
            }
            return count(scandir($dir, SCANDIR_SORT_NONE)) === 2;
        };

        foreach ($this->INSTALL_PATHS as $path) {
            $files = $this->getRecursiveFiles($path);
            $dirs = $this->getRecursiveDirs($path);

            foreach ($files as $from => $to) {
                unlink($to);
            }

            foreach ($dirs as $dir) {
                if ($isDirEmpty($dir)) {
                    if (!rmdir($dir)) {
                        throw new FileDeleteException($dir);
                    }
                }
            }
        }

        return true;
    }

    function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installDB();
        $this->installFiles();
        $this->installEvents();
    }

    function DoUninstall()
    {
        $this->uninstallDB();
        $this->uninstallFiles();
        $this->unInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}

?>